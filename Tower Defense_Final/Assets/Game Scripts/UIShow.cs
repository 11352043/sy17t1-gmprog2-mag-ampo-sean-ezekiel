﻿using UnityEngine;
using System.Collections;

public class UIShow : MonoBehaviour 
{
    public GameObject panel;
    public UISelector[] uiSelectors;
    public Vector3 offset;

	// Use this for initialization
	void Start () 
    {
        panel.SetActive(false);
        uiSelectors = Resources.FindObjectsOfTypeAll<UISelector>();
    }
	
	// Update is called once per frame
	void Update ()
    {

	}
  
    void OnMouseDown()
    {
        for(int i = 0; i < uiSelectors.Length; i++)
        {
            uiSelectors[i].TowerPosition = transform.position + offset;
        }
        panel.SetActive(true);
    }
    

 
}
