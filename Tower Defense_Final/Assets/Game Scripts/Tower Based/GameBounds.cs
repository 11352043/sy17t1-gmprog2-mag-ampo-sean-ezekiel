﻿using UnityEngine;
using System.Collections;

public class GameBounds : MonoBehaviour {

	private float minX = -50;
	private float maxX = 50;
	private float minY = -50;
	private float maxY = 50;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (Mathf.Clamp (transform.position.x, minX, maxX), Mathf.Clamp (transform.position.y, minY, maxY), 22.0f);
	}
}
