﻿using UnityEngine;
using System.Collections;

public class DotBullet : Bullet
{
    protected override void OnEnemyHit(GameObject hit)
    {
        hit.GetComponent<BurningEnemy>().PerformBehavior();
    }
}
