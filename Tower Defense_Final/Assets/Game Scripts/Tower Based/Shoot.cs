﻿ using UnityEngine;
 using System.Collections;

public class Shoot : MonoBehaviour
{
    public Transform BulletSpawnPoint;
    public Transform Enemy;
    public float range = 50.0f;
    public float bulletImpulse = 20.0f;
    public Rigidbody projectile;

    private bool onRange = false;
    private float initialRate;
    public float timeDown;


    void Start()
    {
        //float rand = Random.Range (1.0f, 2.0f);
        //InvokeRepeating ("ShootEnemy", 2, rand);
        initialRate = timeDown;
        
    }

    void ShootEnemy()
    {
        Rigidbody bullet = Instantiate(projectile, BulletSpawnPoint.position, transform.rotation) as Rigidbody;
        bullet.velocity = (Enemy.position - transform.position).normalized * bulletImpulse;

        Destroy(bullet.gameObject, 0.5f);

    }

    void Update()
    {
        if (Enemy != null && initialRate > 0)
        {
            initialRate -= Time.deltaTime;
        }
        if (initialRate <= 0 && Enemy != null)
        {
            onRange = Vector3.Distance(transform.position, Enemy.transform.position) < range;
            if (onRange)
            {
                ShootEnemy();
                initialRate = timeDown;
            }

        }

    }
    void OnTriggerStay(Collider other)
    {

    }

}

