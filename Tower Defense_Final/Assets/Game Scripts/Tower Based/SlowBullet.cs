﻿using UnityEngine;
using System.Collections;

public class SlowBullet : Bullet
{
    protected override void OnEnemyHit(GameObject hit)
    {
        base.OnEnemyHit(hit);
        hit.transform.GetComponent<EnemyPathing>().enemySpeed -= 1f;
        if (hit.transform.GetComponent<EnemyPathing>().enemySpeed == 0f)
        {
            hit.transform.GetComponent<EnemyPathing>().enemySpeed = 1f;
        }
    }
}
