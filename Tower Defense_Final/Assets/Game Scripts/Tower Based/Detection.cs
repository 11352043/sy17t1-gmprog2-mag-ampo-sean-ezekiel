﻿using UnityEngine;
using System.Collections;

public class Detection : MonoBehaviour
{
	public GameObject target;
	public GameObject[] enemies;
    public Enemytypes TargetType;
	public float range = 50.0f;
	private bool onRange = false;

	// Use this for initialization
	void Start ()
	{

	}
	// Update is called once per frame
	void Update ()
	{
		EnemyDist ();
	}

	void EnemyDist ()
	{
		enemies = GameObject.FindGameObjectsWithTag ("Enemy");


		int closestIndex = 0;
		float nearestDistance = Mathf.Infinity;
		float closestEnemy;
		for (int i = 0; i < enemies.Length; i++)
        {
            onRange = Vector3.Distance (transform.position, enemies [i].transform.position) < range;
			closestEnemy = Vector3.Distance (transform.position, base.transform.position);

			if (closestEnemy < nearestDistance && onRange)
            {
				// Debug.Log ("Enemy Spawned",target);
				nearestDistance = closestEnemy;
				closestIndex = i;
				target = enemies [i];
                
                // Target Enemy Types
                if(target.GetComponent<EnemyType>().Type != TargetType)
                {
                    target = null;
                   
                    
                    return;
                }

                transform.LookAt(target.transform);
            }
            
		}
		if (target != null)
			GetComponent<Shoot> ().Enemy = target.transform;
		else
			GetComponent<Shoot> ().Enemy = null;
	}
}
