﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour 
{
    public static int currentLevel = 0;

    private GameManager gameManager;

    void Start()
    {
        GameObject gm = GameObject.Find("GameManager");
        gameManager = gm.GetComponent<GameManager>();
    }

	// Use this for initialization
	public static void RestartLevel ()
    {
		Application.LoadLevel (currentLevel);
	}

}
