﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class Ability 
{
    private BasicObjectInformation objectInfo;
    private List<AbilityEffects> behaviors;

    private bool requiresTarget;
    private bool canCastOnself;
    private float cooldown; //secs
    private GameObject particleEffect; //needs assigned when we create the ability

    public float castTime; //secs
    private float cost;
    private AbilityType type;

    public enum AbilityType
    {
        Spell,
        Melee

    }
    public Ability(BasicObjectInformation aBasicInfo)
    {
        objectInfo = aBasicInfo;
        behaviors = new List<global::AbilityEffects>();
        cooldown = 0;
        requiresTarget = false;
        canCastOnself = false;


    }

    public Ability(BasicObjectInformation aBasicInfo,List<AbilityEffects> abehaviors)
    {
        objectInfo = aBasicInfo;
       
        behaviors = new List<AbilityEffects>();
        behaviors = abehaviors;
        cooldown = 0;
        requiresTarget = false;
        canCastOnself = false;
       

    }

    public Ability(BasicObjectInformation aBasicInfo,List<AbilityEffects> abehaviors ,bool arequiresTarget, int acooldown,GameObject aparticleE)
    {
        objectInfo = aBasicInfo;
        behaviors = new List<AbilityEffects>();
        behaviors = abehaviors;
        cooldown = acooldown;
        requiresTarget = arequiresTarget;
        canCastOnself = false;
       
        particleEffect = aparticleE;

    }
    public BasicObjectInformation AbilityInfo
    {
        get { return objectInfo; }
    }
    public float AbilityCooldown
    {
        get { return cooldown; }
    }
    public List<AbilityEffects> AbilityEffects
    {
        get { return behaviors; }
    }
    //this is the method that will be called anytime we use ability
    public void UseAbility()
    {

    }
        
   

    
	
	
}
