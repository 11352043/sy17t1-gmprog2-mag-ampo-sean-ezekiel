﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Diagnostics;

public class AbilityUse : MonoBehaviour 
{

    public GameObject godPower;
    private Stopwatch abilityCooldownTimer;
    private Button button;
    private Image fillImage;
    
    public void OnAbilityUse(GameObject btn)
    {
        //if ability is not on cool down use it
        fillImage = btn.transform.GetChild(0).gameObject.GetComponent<Image>();
        UnityEngine.Debug.Log(btn.transform.GetChild(0).gameObject.name);
        button = btn.GetComponent<Button>();
        button.interactable = false;
        fillImage.fillAmount = 1;
        abilityCooldownTimer = new Stopwatch();
        abilityCooldownTimer.Start();

        GameObject go = Instantiate<GameObject>(godPower);

        StartCoroutine(SpinImage());

            
    }
    private IEnumerator SpinImage()
    {
        {
            UnityEngine.Debug.Log(fillImage.fillAmount);
            yield return null;
        }
        fillImage.fillAmount = 0;
        button.interactable = true;
        abilityCooldownTimer.Stop();
        abilityCooldownTimer.Reset();

        yield return null;
    }
}
