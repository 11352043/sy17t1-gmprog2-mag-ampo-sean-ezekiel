﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AbilityEffects : MonoBehaviour
{
    private BasicObjectInformation objectInfo;
    private BehaviorStartTimes startTime;

    public AbilityEffects(BasicObjectInformation basicInfo, BehaviorStartTimes sTime)
    {
        objectInfo = basicInfo;
        startTime = sTime;
    }

    public enum BehaviorStartTimes
    {
        Beginning,
        Middle,
        End
    }


    //gameobject,our target,
    public virtual void PerformBehavior(GameObject playerObject,GameObject objectHit)
    {
        Debug.LogWarning("Need to add Effect");
    }

    public BasicObjectInformation AbilityEffectInfo
    {
        get { return objectInfo; }
    }

    public BehaviorStartTimes AbilityEffectStartTime
    {
        get { return startTime; }
    }
	
}
