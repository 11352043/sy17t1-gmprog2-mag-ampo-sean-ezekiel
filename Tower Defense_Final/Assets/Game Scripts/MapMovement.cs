﻿using UnityEngine;
using System.Collections;

public class MapMovement : MonoBehaviour 
{

    public float cameraSpeed = 20.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () 
    {
		this.transform.position = new Vector3 
			(Mathf.Clamp (this.transform.position.x, 0f, 40f),
				Mathf.Clamp (this.transform.position.y, 0f, 5f),
				Mathf.Clamp (this.transform.position.z, 0f, 40f));
        transform.Translate(Vector3.left * Input.GetAxis("Horizontal") * cameraSpeed * Time.deltaTime);
        transform.Translate(Vector3.back * Input.GetAxis("Vertical") * cameraSpeed * Time.deltaTime);
	}
}