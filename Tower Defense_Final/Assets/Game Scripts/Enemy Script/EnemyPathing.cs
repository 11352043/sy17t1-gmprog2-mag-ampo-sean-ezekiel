﻿using UnityEngine;
using System.Collections;

public class EnemyPathing : MonoBehaviour 
{
    public Transform[] EnemyPath;
    public float enemySpeed = 3.0f;
    public int currentPath;

    private float lastWayPointSwitchTime;

    

	// Use this for initialization
	void Start () 
    {
        transform.position = EnemyPath[0].position;
        currentPath = 0;
        lastWayPointSwitchTime = Time.time;
	}
	
	// Update is called once per frame
	void Update ()
    {
        // 1
        Vector3 startingPosition = EnemyPath[currentPath].transform.position;
        Vector3 endPosition = EnemyPath[currentPath + 1].transform.position;
        //2
        float pathLength = Vector3.Distance(startingPosition, endPosition);
        float totalTimeforPath = pathLength / enemySpeed;
        float currentTimeonPath = Time.time - lastWayPointSwitchTime;
        gameObject.transform.position = Vector3.Lerp(startingPosition, endPosition, currentTimeonPath / totalTimeforPath);

        //3
        if (gameObject.transform.position.Equals(endPosition))
        {
            // Second to the last Point
            if (currentPath < EnemyPath.Length - 2)
            {
                currentPath++;
                lastWayPointSwitchTime = Time.time;

                RotateDirection();
            }
            else
            {
                //After reaching the End Point
                Destroy(gameObject);

                GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
                gameManager.Health -= 1;
            }
        }
        transform.LookAt(EnemyPath[currentPath].position);

	}

    private void RotateDirection()
    {
        Vector3 newStartPosition = EnemyPath[currentPath].transform.position;
        Vector3 newEndPosition = EnemyPath[currentPath].transform.position;
        Vector3 newDirection = EnemyPath[currentPath].transform.position;

        float x = newDirection.x;
        float y = newDirection.y;
        float rotationAngle = Mathf.Atan2(y, x) * 180 / Mathf.PI;
    }

    public float GoalDistance()
    {
        float distance = 0;
        distance += Vector3.Distance(gameObject.transform.position, EnemyPath[currentPath + 1].transform.position);
        for (int i = currentPath + 1; i< EnemyPath.Length - 1; i++)
        {
            Vector3 startPosition = EnemyPath[i].transform.position;
            Vector3 endPosition = EnemyPath[i + 1].transform.position;
            distance += Vector3.Distance(startPosition, endPosition);
        }
        return distance;
    }
}