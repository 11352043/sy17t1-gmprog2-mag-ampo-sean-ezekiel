﻿using UnityEngine;
using System.Collections;

public class BurnAllEnemy : MonoBehaviour
{
    public int Damage;

    private GameManager gameManager;
    // Use this for initialization
    void Start()
    {
        GameObject gm = GameObject.Find("GameManager");
        gameManager = gm.GetComponent<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            col.GetComponent<BurningEnemy>().PerformBehavior();
            col.transform.GetComponent<EnemyHealthScript>().Hit(Damage);
        }
    }
}
