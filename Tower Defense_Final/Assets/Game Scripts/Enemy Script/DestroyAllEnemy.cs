﻿using UnityEngine;
using System.Collections;

public class DestroyAllEnemy : MonoBehaviour 
{
    public int Damage;
    private GameManager gameManager;
	// Use this for initialization
	void Start ()
    {
        GameObject gm = GameObject.Find("GameManager");
        gameManager = gm.GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	void Update ()
    {
	
	}
    void OnTriggerStay(Collider col)
    {
        if (col.gameObject.tag == "Enemy")
        {
            col.transform.GetComponent<EnemyHealthScript>().Hit(Damage);
            Destroy(col.gameObject);
            gameManager.Gold += 50;
        }
    }
}
