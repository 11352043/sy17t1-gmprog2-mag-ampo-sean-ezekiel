﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;

public class BurningEnemy : AbilityEffects
{

    private const string abName = "Damage over Time";
    private const string abDescription = "a DOT!";
    private const BehaviorStartTimes startTime = BehaviorStartTimes.Beginning; // on impact
                                                                               // private const Sprite icon = Resources.Load();
    public float effectDuration; // how long the effect lasts
    private Stopwatch durationTimer;
    private float baseEffectDamage;
    public float damageTickDuration;

    public BurningEnemy(float ed, float bd, float dtd)
        : base(new BasicObjectInformation(abName, abDescription), startTime)//can add sprite besied description if have icon
    {
        baseEffectDamage = bd;
        damageTickDuration = dtd;

    }
    void Start()
    {
        durationTimer = new Stopwatch();
    }
    public void PerformBehavior()
    { 
        StartCoroutine((DOT(gameObject)));
    }

    private IEnumerator DOT(GameObject hit)
    {
        durationTimer.Start(); // turns on timer
        while (durationTimer.Elapsed.TotalSeconds <= effectDuration)
        {
            hit.transform.GetComponent<EnemyHealthScript>().Hit(5 );
            UnityEngine.Debug.Log("Burn");
            yield return new WaitForSeconds(damageTickDuration);

        }
        durationTimer.Stop();
        durationTimer.Reset();
        yield return null;
    }
}
