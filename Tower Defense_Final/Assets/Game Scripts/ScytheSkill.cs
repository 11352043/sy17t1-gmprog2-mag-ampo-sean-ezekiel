﻿using UnityEngine;
using System.Collections;

public class ScytheSkill : MonoBehaviour
{
    public GameObject Enemy;
    private GameManager gameManager;

    // Use this for initialization
    void Start ()
    {
        Enemy = GameObject.FindGameObjectWithTag("Enemy");
        GameObject gm = GameObject.Find("GameManager");
        gameManager = gm.GetComponent<GameManager>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        transform.position = Vector3.MoveTowards(transform.position, Enemy.transform.position, 0.1f);
        transform.LookAt(Enemy.transform.position);
	}
    void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        Destroy(this.gameObject);
        gameManager.Gold += 100;
    }
}
