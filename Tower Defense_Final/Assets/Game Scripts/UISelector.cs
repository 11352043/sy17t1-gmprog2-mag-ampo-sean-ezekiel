﻿using UnityEngine;
using System.Collections;

public class UISelector : MonoBehaviour
{
    public GameObject panel;
    public GameObject TowerPrefab;
    public Vector3 TowerPosition;
    private GameObject Tower;
    private GameManager gameManager;

   
	void Start ()
    {
       gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	void Update ()
    {
	}

    public void CreateTower(GameObject btn)
    {
        Tower = (GameObject)Instantiate(TowerPrefab, TowerPosition, Quaternion.identity);
        TowerCost();
        panel.SetActive(false);
    }

    private bool TowerCost()
    {
        int cost = TowerPrefab.GetComponent<TowerData>().levels[0].cost;
        if (gameManager.Gold < cost)
        {
            Destroy(Tower);
        }
        else
        {
            gameManager.Gold -= Tower.GetComponent<TowerData>().CurrentLevel.cost;
        }
        
        return Tower == null && gameManager.Gold >= cost;
    }
}
 