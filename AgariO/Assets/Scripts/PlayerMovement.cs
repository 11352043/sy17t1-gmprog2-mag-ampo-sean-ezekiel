﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
    public float Speed;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
       
        Vector3 movement = mousePos - transform.position;
        movement.z = transform.position.z;
        Debug.Log(movement);

        movement.Normalize(); // Makes the vector length 1

        if (Vector2.Distance(mousePos, transform.position) < 0.1f)
            return;

        transform.Translate(Speed * movement * Time.deltaTime);

        //if (Vector2.Distance(mousePos, transform.position) > 0.1f)
        //{
        //    transform.Translate(Speed * movement * Time.deltaTime);
        //}
    }
}
