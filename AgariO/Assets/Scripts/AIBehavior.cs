﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIBehavior : MonoBehaviour {
    public enum AIState
    {
        ChasePlayer,
        EatFood
    }

    AIState currentState;
	// Use this for initialization
	void Start () {
        currentState = AIState.EatFood;
	}
	
	// Update is called once per frame
	void Update () {
        if (currentState == AIState.ChasePlayer)
            ChaseState();
        else if (currentState == AIState.EatFood)
            EatFoodState();	
	}

    void ChaseState()
    {
        // Distance
        // if distance of this object to player < someValue
        // float dist = Vector3.Distance(transform.postion, player.transform.position); // Assign player
        // transform.Translate towards player
        // && if playerSize < mySize

        // if condition above is not met
        // currentState = AIState.EatFood;
    }

    void EatFoodState()
    {
        // Find nearest food object
        // Find GameObjectsWithTag
        // Sort by distance
        // Get nearest
        // transform.Translate(to nearest food) / or Vector3.MoveTowards
    }
}
