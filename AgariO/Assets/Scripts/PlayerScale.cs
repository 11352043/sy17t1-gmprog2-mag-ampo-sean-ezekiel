﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScale : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.KeypadPlus))
        {
            this.transform.localScale += new Vector3(1.0f * Time.deltaTime, 1.0f * Time.deltaTime, 1.0f * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.KeypadMinus))
        {
            this.transform.localScale += new Vector3(-1.0f * Time.deltaTime, -1.0f * Time.deltaTime, -1.0f * Time.deltaTime);
        }
    }
}