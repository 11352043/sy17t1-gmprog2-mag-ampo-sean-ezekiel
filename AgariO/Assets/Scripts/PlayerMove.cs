﻿using UnityEngine;
using System.Collections;

public class PlayerMove : MonoBehaviour
{
    public float speed;
    private Vector3 player;

    // Use this for initialization
    void Start()
    {
        player = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        player = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        player.z = transform.position.z;

        transform.position = Vector3.MoveTowards(transform.position, player, speed * Time.deltaTime);

    }
}