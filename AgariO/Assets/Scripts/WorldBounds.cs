﻿using UnityEngine;
using System.Collections;

public class WorldBounds : MonoBehaviour
{

    void Update()
    {
        Vector3 PlayerLock = transform.position;
        PlayerLock.x = Mathf.Clamp(transform.position.x, -15.0f, 15.0f);
        PlayerLock.y = Mathf.Clamp(transform.position.y, -15.0f, 15.0f);
        transform.position = PlayerLock;

    }

}