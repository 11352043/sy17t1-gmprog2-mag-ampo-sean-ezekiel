﻿using UnityEngine;
using System.Collections;

public class EnemSpawner : MonoBehaviour
{
    public GameObject EnemyPrefab;
    public Vector3 MinSpawnPoint;
    public Vector3 MaxSpawnPoint;
    public float MinSpawnInterval = 1.0f;
    public float MaxSpawnInterval = 3.0f;

    void Start()
    {
        for (int x = 0; x < 30; x++)
        {
            SpawnEnemy();
        }
        StartCoroutine(SpawnTask());
    }

    IEnumerator SpawnTask()
    {
        while (true)
        {
            yield return new WaitForSeconds
            (Random.Range(MaxSpawnInterval, MinSpawnInterval));
            SpawnEnemy();
        }
    }
    void SpawnEnemy()
    {
        Debug.Log("Spawned");
        GameObject enemy = Instantiate(EnemyPrefab);

        float x = Random.Range(MinSpawnPoint.x, MaxSpawnPoint.y);
        float y = Random.Range(MinSpawnPoint.x, MaxSpawnPoint.y);
        float z = Random.Range(MinSpawnPoint.z, MaxSpawnPoint.z);
        enemy.transform.position = new Vector3(x, y, z);
    }

}