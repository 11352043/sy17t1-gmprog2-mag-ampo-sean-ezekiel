﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamFollow : MonoBehaviour
{

    public GameObject Player;
    public float Speed;
    private Vector3 offset;
    // Use this for initialization
    void Start()
    {
        offset = transform.position - Player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
            transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, Speed * Time.deltaTime);
            Vector3 Lock = transform.position;
            Lock.z = Mathf.Clamp(transform.position.z, -10, -10);
            transform.position = Lock;
    }
}