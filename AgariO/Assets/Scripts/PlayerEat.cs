﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerEat : MonoBehaviour
{
    //public string Tag;
    public Text Letters;
    public float Increase;

    int Score = 0;
    void update()
    {
        Letters.text = "SCORE: " + Score;
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "food")
        {
            transform.localScale += new Vector3(Increase, Increase, Increase);
            Destroy(other.gameObject);

            Score += 10;
            Letters.text = "SCORE: " + Score;
        }
    }

}